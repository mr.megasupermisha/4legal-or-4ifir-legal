# 4iLegal or 4IFIR LEGAL



## Getting started
English:
Hello everyone. This version is 4IFIR with the Atmosphere launch capability disabled, thereby eliminating the possibility of piracy. Additionally, the link to the source code of 4IFIR is provided here, making this project entirely legal. ATTENTION! ANY MODIFICATION OF SYSTEM FILES IS YOUR RESPONSIBILITY!

Russian:
Всем привет. Данная версия является 4IFIR с отключённой возможностью запускать Atmoshere, следовательно, делает невозможность пиратства. Также здесь приложена ссылка на исходный код 4IFIR, что делает данный проект абсолютно легальным. ВНИМАНИЕ! ЛЮБАЯ МОДИФИКАЦИЯ СИСТЕМНЫХ ФАЙЛОВ ПОД ВАШЕЙ ОТВЕТСТВЕННОСТЬЮ!
## Links
Github with instructions of 4IFIR https://github.com/rashevskyv/4IFIR

Official telegram chat of Kefir and 4IFIR https://t.me/kefir_switch 


## License
GNU GPL V2

